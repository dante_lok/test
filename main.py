import json
import os
import time
import asyncio
import requests


async def handle():
    username = "c0a9106e440d5b2c81a73cbfe7919efb"
    password = "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"
    api_version = "2021-01"
    resource = "products"
    url = "https://{}:{}@example2dante.myshopify.com/admin/api/{}/{}.json".format(username, password, api_version, resource)
    request = requests.get(url)
    data = request.json()
    return data

async def main_coroutine():
    coroutine_object = handle()
    result = await coroutine_object
    return result

st = time.time()
data = asyncio.run(main_coroutine())
print(f"Asynchronous exec took {time.time() - st} seconds")
print(data)
